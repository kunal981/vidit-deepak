//
//  SignUpViewController.h
//  VIDIT
//
//  Created by brst on 30/06/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *txtName;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
- (IBAction)btnRegisterPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *registerbtn;

@end
