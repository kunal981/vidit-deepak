//
//  ForgotViewController.m
//  VIDIT
//
//  Created by brst on 27/07/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface ProfileView : UITableViewController <UIActionSheetDelegate>
//-------------------------------------------------------------------------------------------------------------------------------------------------

- (id)initWith:(NSString *)userId_;

@end
