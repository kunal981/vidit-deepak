//
//  ForgotViewController.h
//  VIDIT
//
//  Created by brst on 27/07/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotViewController : UIViewController<UITextFieldDelegate>
- (IBAction)btnSubmit:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;


@end
