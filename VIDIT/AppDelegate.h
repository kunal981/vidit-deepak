//
//  AppDelegate.h
//  VIDIT
//
//  Created by brst on 4/4/15.
//  Copyright (c) 2015 brst. All rights reserved.
//#import "ContactListViewController.h"

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

#import "SignUpViewController.h"
#import "NavigationController.h"
#import "RecentView.h"
#import "GroupsView.h"
#import "PeopleView.h"
#import "PeopleAddressView.h"
#import "SettingsView.h"
NSString *deviceID;
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) UITabBarController *tabBarController;

@property (strong, nonatomic) RecentView *recentView;
@property (strong, nonatomic) GroupsView *groupsView;
//@property (strong, nonatomic) PeopleView *peopleView;
@property (strong, nonatomic) PeopleAddressView *peopleAddressView;
@property (strong, nonatomic) SettingsView *settingsView;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

