//
//  ForgotViewController.m
//  VIDIT
//
//  Created by brst on 27/07/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

//-------------------------------------------------------------------------------------------------------------------------------------------------
@protocol SelectMultipleDelegate
//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)didSelectMultipleUsers:(NSMutableArray *)users;

@end

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface SelectMultipleView : UITableViewController
//-------------------------------------------------------------------------------------------------------------------------------------------------

@property (nonatomic, assign) IBOutlet id<SelectMultipleDelegate>delegate;

@end
