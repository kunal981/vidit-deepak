//
//  ForgotViewController.m
//  VIDIT
//
//  Created by brst on 27/07/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <Parse/Parse.h>

//-------------------------------------------------------------------------------------------------------------------------------------------------
void			ParsePushUserAssign		(void);
void			ParsePushUserResign		(void);

//-------------------------------------------------------------------------------------------------------------------------------------------------
void			SendPushNotification	(NSString *groupId, NSString *text);
