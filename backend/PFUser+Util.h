//
//  ForgotViewController.m
//  VIDIT
//
//  Created by brst on 27/07/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <Parse/Parse.h>

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface PFUser (Util)
//-------------------------------------------------------------------------------------------------------------------------------------------------

- (NSString *)fullname;

- (BOOL)isEqualTo:(PFUser *)user;

@end
