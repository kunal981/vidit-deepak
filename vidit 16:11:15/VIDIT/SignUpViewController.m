//
//  SignUpViewController.m
//  VIDIT
//
//  Created by brst on 30/06/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "SignUpViewController.h"
#import "ProgressHUD.h"
#import <Parse/Parse.h>
#import "AppConstant.h"
#import "push.h"
#import "RecentView.h"

@interface SignUpViewController ()

@end

@implementation SignUpViewController
@synthesize txtEmail,txtName,txtPassword;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Register";
    _registerbtn.backgroundColor = [UIColor colorWithRed:(0/255.0f) green:(173/255.0f) blue:(239/255.0f) alpha:1.0f];

    self.txtName.layer.borderWidth = 1.0;
    self.txtName.layer.borderColor = [UIColor grayColor].CGColor;
    self.txtName.layer.cornerRadius = 3.0;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
     self.txtName.leftView = paddingView;
     self.txtName.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtEmail.layer.borderWidth = 1.0;
    self.txtEmail.layer.borderColor = [UIColor grayColor].CGColor;
    self.txtEmail.layer.cornerRadius = 3.0;
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.txtEmail.leftView = paddingView1;
    self.txtEmail.leftViewMode = UITextFieldViewModeAlways;
    
    self.txtPassword.layer.borderWidth = 1.0;
    self.txtPassword.layer.borderColor = [UIColor grayColor].CGColor;
    self.txtPassword.layer.cornerRadius = 3.0;
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.txtPassword.leftView = paddingView2;
    self.txtPassword.leftViewMode = UITextFieldViewModeAlways;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnRegisterPressed:(id)sender {
    
    NSString *name		= txtName.text;
    NSString *password	= txtPassword.text;
    NSString *email		= [txtEmail.text lowercaseString];
    //---------------------------------------------------------------------------------------------------------------------------------------------
    if ([name length] < 3)		{ [ProgressHUD showError:@"Name is too short."]; return; }
    if ([password length] == 0)	{ [ProgressHUD showError:@"Password must be set."]; return; }
    if ([email length] == 0)	{ [ProgressHUD showError:@"Email must be set."]; return; }
    //---------------------------------------------------------------------------------------------------------------------------------------------
    [ProgressHUD show:@"Please wait..." Interaction:NO];
    //---------------------------------------------------------------------------------------------------------------------------------------------
    PFUser *user = [PFUser user];
    user.username = email;
    user.password = password;
    user.email = email;
    user[PF_USER_EMAILCOPY] = email;
    user[PF_USER_FULLNAME] = name;
    user[PF_USER_FULLNAME_LOWER] = [name lowercaseString];
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
     {
         if (error == nil)
         {
              ParsePushUserAssign();
             [ProgressHUD showSuccess:@"Succeed."];
            // RecentView *recentVC = [[RecentView alloc]init];
             [self dismissViewControllerAnimated:YES completion:nil];
         }
         else [ProgressHUD showError:error.userInfo[@"error"]];
     }];

}

//-(void)actionRegister{
//    NSString *name		= txtName.text;
//    NSString *password	= txtPassword.text;
//    NSString *email		= [txtEmail.text lowercaseString];
//    //---------------------------------------------------------------------------------------------------------------------------------------------
//    if ([name length] < 8)		{ [ProgressHUD showError:@"Name is too short."]; return; }
//    if ([password length] == 0)	{ [ProgressHUD showError:@"Password must be set."]; return; }
//    if ([email length] == 0)	{ [ProgressHUD showError:@"Email must be set."]; return; }
//    //---------------------------------------------------------------------------------------------------------------------------------------------
//    [ProgressHUD show:@"Please wait..." Interaction:NO];
//    //---------------------------------------------------------------------------------------------------------------------------------------------
//    PFUser *user = [PFUser user];
//    user.username = email;
//    user.password = password;
//    user.email = email;
//    user[PF_USER_EMAILCOPY] = email;
//    user[PF_USER_FULLNAME] = name;
//    user[PF_USER_FULLNAME_LOWER] = [name lowercaseString];
//    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
//     {
//         if (error == nil)
//         {
//             ParsePushUserAssign();
//             [ProgressHUD showSuccess:@"Succeed."];
//             [self dismissViewControllerAnimated:YES completion:nil];
//         }
//         else [ProgressHUD showError:error.userInfo[@"error"]];
//     }];
//}
#pragma mark - UITextField delegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (BOOL)textFieldShouldReturn:(UITextField *)textField
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    if (textField == txtName)
    {
        [txtPassword becomeFirstResponder];
    }
    if (textField == txtPassword)
    {
        [txtEmail becomeFirstResponder];
    }
    if (textField == txtEmail)
    {
        [textField resignFirstResponder];;
    }
    return YES;
}


@end
