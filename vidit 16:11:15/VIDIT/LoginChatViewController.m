//
//  LoginChatViewController.m
//  VIDIT
//
//  Created by brst on 08/07/15.
//  Copyright (c) 2015 brst. All rights reserved.
//
#import <Parse/Parse.h>
#import "ProgressHUD.h"

#import "AppConstant.h"
#import "push.h"
#import "LoginChatViewController.h"
#import "SignUpViewController.h"
#import "ForgotViewController.h"

@interface LoginChatViewController ()

@end

@implementation LoginChatViewController
@synthesize fieldEmail,fieldPassword;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"VIDIT";
    // Do any additional setup after loading the view from its nib.
    _btnLogin.backgroundColor = [UIColor colorWithRed:(0/255.0f) green:(173/255.0f) blue:(239/255.0f) alpha:1.0f];
    _btnRegister.backgroundColor = [UIColor colorWithRed:(0/255.0f) green:(173/255.0f) blue:(239/255.0f) alpha:1.0f];
    fieldEmail.layer.borderWidth = 1.0;
    fieldEmail.layer.borderColor = [UIColor lightGrayColor].CGColor;
    fieldEmail.layer.cornerRadius = 3.0;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    fieldEmail.leftView = paddingView;
    fieldEmail.leftViewMode = UITextFieldViewModeAlways;
    
    fieldPassword.layer.borderWidth = 1.0;
    fieldPassword.layer.borderColor = [UIColor lightGrayColor].CGColor;
    fieldPassword.layer.cornerRadius = 3.0;
    
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
     fieldPassword.leftView = paddingView1;
     fieldPassword.leftViewMode = UITextFieldViewModeAlways;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnLoginPressed:(id)sender {
    NSString *email = [fieldEmail.text lowercaseString];
    NSString *password = fieldPassword.text;
    //---------------------------------------------------------------------------------------------------------------------------------------------
    if ([email length] == 0)	{ [ProgressHUD showError:@"Email must be set."]; return; }
    if ([password length] == 0)	{ [ProgressHUD showError:@"Password must be set."]; return; }
    //---------------------------------------------------------------------------------------------------------------------------------------------
    [ProgressHUD show:@"Signing in..." Interaction:NO];
    [PFUser logInWithUsernameInBackground:email password:password block:^(PFUser *user, NSError *error)
     {
         if (user != nil)
         {
             ParsePushUserAssign();
             [ProgressHUD showSuccess:[NSString stringWithFormat:@"Welcome back to vidit messenger"]];
             [self dismissViewControllerAnimated:YES completion:nil];
         }
         else [ProgressHUD showError:error.userInfo[@"error"]];
     }];

    
}
- (IBAction)btnRegisterPressed:(id)sender {
    SignUpViewController *registerView = [[SignUpViewController alloc] init];
    [self.navigationController pushViewController:registerView animated:YES];
}

- (IBAction)btnForgotPassword:(id)sender {
    ForgotViewController *forgotVC = [[ForgotViewController alloc]init];
    [self.navigationController pushViewController:forgotVC animated:YES];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
@end
