//
//  CustomTableViewCell.h
//  VIDIT
//
//  Created by brst on 01/09/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>

#import "AppConstant.h"
#import "converter.h"

 extern NSMutableArray *post_objectIDArray;
extern BOOL like;
@interface CustomTableViewCell : UITableViewCell{
    PFObject *recent;
    NSString *post_object_id;
    NSMutableArray *post_objectIDArray;
   
}
@property (strong, nonatomic) IBOutlet PFImageView *imageUser;
@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UILabel *lblUserName;
@property (strong, nonatomic) IBOutlet UITextView *txtViewChating;
@property (strong, nonatomic) IBOutlet UILabel *lblLikes;
@property (strong, nonatomic) IBOutlet UIButton *btnLike;
@property (strong, nonatomic) IBOutlet UILabel *lblComment;
@property (strong, nonatomic) IBOutlet UIButton *btnComment;

- (void)getData:(PFObject *)recent_;
@end
