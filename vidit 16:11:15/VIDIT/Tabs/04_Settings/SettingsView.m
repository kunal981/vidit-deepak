

//
//  ForgotViewController.m
//  VIDIT
//
//  Created by brst on 27/07/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import "ProgressHUD.h"

#import "AppConstant.h"
#import "camera.h"
#import "common.h"
#import "image.h"
#import "push.h"

#import "SettingsView.h"
#import "BlockedView.h"
#import "PrivacyView.h"
#import "TermsView.h"
#import "NavigationController.h"
#import "GKImagePicker.h"
#import "CustomIOSAlertView.h"
#import "CustomTableViewCell.h"
#import "CommentViewController.h"


//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface SettingsView()<GKImagePickerDelegate,CustomIOSAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UITextView *textViewStatus;
    
@property (strong, nonatomic) IBOutlet UIView *FooterView;

@property (nonatomic, strong) GKImagePicker *imagePicker;
@property (nonatomic, strong) UIPopoverController *popoverController;

@property (strong, nonatomic) IBOutlet UIView *viewHeader;
@property (strong, nonatomic) IBOutlet PFImageView *imageUser;
@property (strong, nonatomic) IBOutlet UILabel *labelName;

@property (strong, nonatomic) IBOutlet UITableViewCell *cellBlocked;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellPrivacy;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellTerms;

@property (strong, nonatomic) IBOutlet UITableViewCell *cellLogout;
@property (strong, nonatomic) IBOutlet UITableView *liveFeedTableView;
@property (strong, nonatomic) IBOutlet UILabel *lblWhatIs;

@end
//-------------------------------------------------------------------------------------------------------------------------------------------------

@implementation SettingsView
@synthesize popoverController;
@synthesize viewHeader, imageUser, labelName;
@synthesize cellBlocked, cellPrivacy, cellTerms, cellLogout;
@synthesize imageView,txtView,labelUserName,CustomChatCell;

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self)
	{
		[self.tabBarItem setImage:[UIImage imageNamed:@"tab_settings"]];
		self.tabBarItem.title = @"Settings";
	}
	return self;
}
        
//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)viewDidLoad
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[super viewDidLoad];
    likeArray = [[NSMutableArray alloc]init];
    textArray = [[NSMutableArray alloc]init];
   
    NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"Wallpaper"];
    UIImage* image = [UIImage imageWithData:imageData];
    if (image != nil) {
        self.imgView_wallpaper.image = image;
    }
	self.title = @"Settings";
	//---------------------------------------------------------------------------------------------------------------------------------------------
	self.tableView.tableHeaderView = viewHeader;
    self.tableView.tableFooterView = _FooterView;
	//---------------------------------------------------------------------------------------------------------------------------------------------
	imageUser.layer.cornerRadius = imageUser.frame.size.width/2;
	imageUser.layer.masksToBounds = YES;
    //---------------------------------------------------------------------------------------------------------------------------------------------
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapONLabel)];
    tap.numberOfTapsRequired = 1;
    [self.lblWhatIs addGestureRecognizer:tap];
    
    
    _liveFeedTableView.separatorColor = [UIColor clearColor];
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    [_liveFeedTableView registerNib:[UINib nibWithNibName:@"CustomTableViewCell" bundle:nil] forCellReuseIdentifier:@"myCell"];
    //---------------------------------------------------------------------------------------------------------------------------------------------

    
   // [self getlikes];
    
    // Create the post
//    PFObject *myPost = [PFObject objectWithClassName:@"Post"];
//  //  myPost[@"title"] = @"I'm Hungry";
//    myPost[@"content"] = @"Where should we go for lunch?";
//    
//    // Create the comment
//    PFObject *myComment = [PFObject objectWithClassName:@"Comment"];
//    myComment[@"content"] = @"Let's do Sushirrito.";
//    
//    // Add a relation between the Post and Comment
//    myComment[@"parent"] = myPost;
//    
//    // This will save both myPost and myComment
//    [myComment saveInBackground];
//    
//    PFObject *post = myComment[@"parent"];
//    [post fetchIfNeededInBackgroundWithBlock:^(PFObject *post, NSError *error) {
//        NSString *title = post[@"content"];
//        NSLog(@"%@",title);
//        // do something with your title variable
//    }];
//    dispatch_async(dispatch_get_main_queue(), ^{
//    PFUser *user = [PFUser currentUser];
//    PFRelation *relation = [user relationForKey:@"likes"];
//    [relation addObject:post];
//    [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//        if (succeeded) {
//            NSLog(@"success");
//            // The post has been added to the user's likes relation.
//        } else {
//            NSLog(@"error");
//            // There was a problem, check error.description
//        }
//    }];
//        
//        });
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     [self fetchData];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)viewDidAppear:(BOOL)animated
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[super viewDidAppear:animated];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	if ([PFUser currentUser] != nil)
	{
		[self loadUser];
	}
	else LoginUser(self);
}

#pragma mark - Backend actions

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)loadUser
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	PFUser *user = [PFUser currentUser];

	[imageUser setFile:user[PF_USER_PICTURE]];
	[imageUser loadInBackground];

	labelName.text = user[PF_USER_FULLNAME];
}

#pragma mark - User actions

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionBlocked
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	BlockedView *blockedView = [[BlockedView alloc] init];
	blockedView.hidesBottomBarWhenPushed = YES;
	[self.navigationController pushViewController:blockedView animated:YES];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionPrivacy
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	PrivacyView *privacyView = [[PrivacyView alloc] init];
	privacyView.hidesBottomBarWhenPushed = YES;
	[self.navigationController pushViewController:privacyView animated:YES];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionTerms
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	TermsView *termsView = [[TermsView alloc] init];
	termsView.hidesBottomBarWhenPushed = YES;
	[self.navigationController pushViewController:termsView animated:YES];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionCleanup
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	imageUser.image = [UIImage imageNamed:@"settings_blank"];
	labelName.text = nil;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionLogout
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel"
										  destructiveButtonTitle:@"Log out" otherButtonTitles:nil];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        // In this case the device is an iPad.
        [PFUser logOut];
        ParsePushUserResign();
        PostNotification(NOTIFICATION_USER_LOGGED_OUT);
        [self actionCleanup];
        LoginUser(self);
    }
    else{
        // In this case the device is an iPhone/iPod Touch.
        [action showFromTabBar:[[self tabBarController] tabBar]];
        
    }
}

#pragma mark - UIActionSheetDelegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	if (buttonIndex != actionSheet.cancelButtonIndex)
	{
		[PFUser logOut];
		ParsePushUserResign();
		PostNotification(NOTIFICATION_USER_LOGGED_OUT);
		[self actionCleanup];
		LoginUser(self);
	}
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (IBAction)actionPhoto:(id)sender
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	PresentPhotoLibrary(self, YES);
}

#pragma mark - UIImagePickerControllerDelegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	UIImage *image = info[UIImagePickerControllerEditedImage];
    NSLog(@"%@",image);
	//---------------------------------------------------------------------------------------------------------------------------------------------
    
	UIImage *picture = ResizeImage(image, 280, 280);
	UIImage *thumbnail = ResizeImage(image, 60, 60);
	//---------------------------------------------------------------------------------------------------------------------------------------------
	imageUser.image = picture;
	//---------------------------------------------------------------------------------------------------------------------------------------------
	PFFile *filePicture = [PFFile fileWithName:@"picture.jpg" data:UIImageJPEGRepresentation(picture, 0.6)];
	[filePicture saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
	{
		if (error != nil) [ProgressHUD showError:@"Network error."];
	}];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	PFFile *fileThumbnail = [PFFile fileWithName:@"thumbnail.jpg" data:UIImageJPEGRepresentation(thumbnail, 0.6)];
	[fileThumbnail saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
	{
		if (error != nil) [ProgressHUD showError:@"Network error."];
	}];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	PFUser *user = [PFUser currentUser];
	user[PF_USER_PICTURE] = filePicture;
	user[PF_USER_THUMBNAIL] = fileThumbnail;
	[user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
	{
		if (error != nil) [ProgressHUD showError:@"Network error."];
	}];
	//---------------------------------------------------------------------------------------------------------------------------------------------
    
	[picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    wallpaper = false;
}

#pragma mark - Table view data source

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    if (tableView == _liveFeedTableView) return 1;
        
    
	return 2;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    if (tableView == _liveFeedTableView) return textArray.count;
    if (section == 0) return 3;
	if (section == 1) return 1;
	return 0;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    if (tableView == _liveFeedTableView) {
        CustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //[anarray objectAtIndex:(anarray.count - indexPath.row - 1)];
        [cell getData:textArray[ textArray.count-indexPath.row-1]];
        
      
        NSLog(@"LIKE %d",like);
        if (like) {
            [cell.btnLike setTitle:@"Dislike" forState:UIControlStateNormal];
        }
        else{
             [cell.btnLike setTitle:@"Like" forState:UIControlStateNormal];
        }
       
         PFObject *objectID = [textArray objectAtIndex:textArray.count-indexPath.row-1];
         PFQuery *query = [PFQuery queryWithClassName:@"Like"];
        
        [query whereKey:@"post_object_id" equalTo:objectID];
        
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
         {
             if (error == nil)
             {
                 NSLog(@"%lu",(unsigned long)objects.count);
                 if (objects.count !=0) {
                     
                     
                     cell.lblLikes.text = [NSString stringWithFormat:@"%lu Likes",(unsigned long)objects.count];
                     //cell.lblComment.text = [NSString stringWithFormat:@"%lu Comments",(unsigned long)objects.count];
                 }
                 else{
                    cell.lblLikes.text = @"";
                 }
                 
             }
             else [ProgressHUD showError:@"Network error."];
         }];
        
        PFQuery *query1 = [PFQuery queryWithClassName:@"Comment"];
        
        [query1 whereKey:@"post_object_id" equalTo:objectID];
        
        
        [query1 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
         {
             if (error == nil)
             {
                 if (objects.count !=0) {
                     
                     
                   //  cell.lblLikes.text = [NSString stringWithFormat:@"%lu Likes",(unsigned long)objects.count];
                     cell.lblComment.text = [NSString stringWithFormat:@"%lu Comments",(unsigned long)objects.count];
                 }
                 else{
                    cell.lblComment.text = @"";
                 }
                 
             }
             else [ProgressHUD showError:@"Network error."];
         }];

        
        [cell.btnLike addTarget:self action:@selector(btnLikePressed:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnLike.tag = textArray.count-indexPath.row-1;
        
        [cell.btnComment addTarget:self action:@selector(btnCommentPressed:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnComment.tag = textArray.count-indexPath.row-1;

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
	if ((indexPath.section == 0) && (indexPath.row == 0)) return cellBlocked;
	if ((indexPath.section == 0) && (indexPath.row == 1)) return cellPrivacy;
	if ((indexPath.section == 0) && (indexPath.row == 2)) return cellTerms;
	if ((indexPath.section == 1) && (indexPath.row == 0)) return cellLogout;
	return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _liveFeedTableView) {
        return 160;
    }
    else return 60;
}

#pragma mark - Table view delegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (tableView == _liveFeedTableView){
        
    }
    else{
        
    
	//---------------------------------------------------------------------------------------------------------------------------------------------
	if ((indexPath.section == 0) && (indexPath.row == 0)) [self actionBlocked];
	if ((indexPath.section == 0) && (indexPath.row == 1)) [self actionPrivacy];
	if ((indexPath.section == 0) && (indexPath.row == 2)) [self actionTerms];
	if ((indexPath.section == 1) && (indexPath.row == 0)) [self actionLogout];
    }
}

-(void)btnLikePressed:(id)sender{
    UIButton *button = (UIButton *)sender;
    NSString *buttonTitle = button.currentTitle;
    NSLog(@"%@",buttonTitle);
    if ([buttonTitle isEqualToString:@"Like"]) {
        PFObject *objectID = [textArray objectAtIndex:button.tag];
        [self saveLike:objectID Islike:true];
    }
    else {
        PFObject *objectID = [textArray objectAtIndex:button.tag];
        [self saveLike:objectID Islike:false];
    }
    
   
    
    
    
        
}
-(void)btnCommentPressed:(id)sender{
    UIButton *btn = sender;
    NSLog(@"%ld",(long)btn.tag);
    
    PFObject *objectID = [textArray objectAtIndex:btn.tag];
    //[self saveLike:objectID Islike:NO];
    
    CommentViewController * commentVC  =  [[CommentViewController alloc]init];
    commentVC.objID =  objectID;
    [self.navigationController pushViewController:commentVC animated:YES];
    
    
    
    
    
    
}

-(void)saveLike:(PFObject*)post_object_ID  Islike:(BOOL)islike{
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    PFObject *object = [PFObject objectWithClassName:@"Like"];
    object[@"user_object_id"] = [PFUser currentUser];
    object[@"post_object_id"] = post_object_ID;
    //object [@"isLike"] = [NSNumber numberWithBool:islike];
    
    //object[@"isLike"] = islike;
    
    if (islike) {
        [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
         {
             if (error == nil)
             {
                 [_liveFeedTableView reloadData];
                 //[self getlikes];
             }
             else [ProgressHUD showError:@"Network error."];;
         }];
 
    }
    else
    {
        NSString *postID  = [NSString stringWithFormat:@"%@",[post_object_ID objectId]];
        PFQuery *query = [PFQuery queryWithClassName:@"Like"];
        [query whereKey:@"post_object_id" equalTo:post_object_ID];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                // The find succeeded.
                NSLog(@"Successfully retrieved %lu scores.", (unsigned long)objects.count);
                // Do something with the found objects
                for (PFObject *object in objects) {
                    [object deleteInBackground];
                }
            } else {
                // Log details of the failure
                NSLog(@"Error: %@ %@", error, [error userInfo]);
            }
        }];
        
    }
    
    
    
    
    //------------------------------------------------------------------------------------------------
    
    
    NSString *postID  = [NSString stringWithFormat:@"%@",[post_object_ID objectId]];
    PFQuery *query = [PFQuery queryWithClassName:@"Post"];
    
    // Retrieve the object by id
    [query getObjectInBackgroundWithId:postID
                                 block:^(PFObject *post, NSError *error) {
                                     
                                     post[@"isLike"] = [NSNumber numberWithBool:islike];
                                    
                                     [post saveInBackground];
                                     [self fetchData];
                                 }];
    
    //------------------------------------------------------------------------------------------------
}

//-(void)getlikes{
//    NSMutableArray *likeArray = [[NSMutableArray alloc]init];
//    
//    PFQuery *query = [PFQuery queryWithClassName:@"Like"];
//   
//    
//    
//    
//    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
//     {
//         if (error == nil)
//         {
//             if (objects.count !=0) {
//                 [likeArray removeAllObjects];
//                 [likeArray addObjectsFromArray:objects];
//                 
//                 NSLog(@"%lu",(unsigned long)likeArray.count);
//             }
//             
//         }
//         else [ProgressHUD showError:@"Network error."];
//              }];
// 
//}



- (IBAction)btn_addwallpaperClick:(id)sender {
    self.imagePicker = [[GKImagePicker alloc] init];
    self.imagePicker.cropSize = CGSizeMake(self.view.frame.size.width,200 );
    self.imagePicker.delegate = self;
    
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//        
//        self.popoverController = [[UIPopoverController alloc] initWithContentViewController:self.imagePicker.imagePickerController];
//        [self.popoverController presentPopoverFromRect:_btnAdd.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//        
//    } else {
    
        [self presentModalViewController:self.imagePicker.imagePickerController animated:YES];
        
    //}
    
}
# pragma mark -
# pragma mark GKImagePicker Delegate Methods

- (void)imagePicker:(GKImagePicker *)imagePicker pickedImage:(UIImage *)image{
    self.imgView_wallpaper.image = image;
    [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(image) forKey:@"Wallpaper"];
    [self hideImagePicker];
}

- (void)hideImagePicker{
//    if (UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM()) {
//        
//        [self.popoverController dismissPopoverAnimated:YES];
//        
//    } else {
    
        [self.imagePicker.imagePickerController dismissViewControllerAnimated:YES completion:nil];
        
   // }
}


//-------------------------------------------------------------------------------------------------------------------------------------------------
-(void)tapONLabel{
    // Here we need to pass a full frame
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    
    // Add some custom content to the alert view
    [alertView setContainerView:[self createDemoTextView]];
    
    // Modify the parameters
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Cancel", @"Post", nil]];
    [alertView setDelegate:self];
    
    // You may use a Block, rather than a delegate.
//    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
//        NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
//        [alertView close];
//    }];
    
    [alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [alertView show];

}

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", (int)buttonIndex, (int)[alertView tag]);
    if (buttonIndex ==0) {
         [alertView close];
    }
    else if (buttonIndex ==1){
        
        NSString *str  = demoTextView.text;
        PFUser *user = [PFUser currentUser];
        PFFile *file = user[PF_USER_PICTURE];
        if (file==nil) {
            imageView = nil;
            imageView.image =nil;
            // [self actionChat:recent[PF_RECENT_GROUPID]];
        }else {
            
            [file getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                image_user = [UIImage imageWithData:data];
                NSString *name = user[PF_USER_FULLNAME];
                
                [self savePost:str Picture:image_user Name:name];

                
            }];
        }
             //[textArray addObject:str];
       
        [alertView close];
    }
   
}


-(UITextView *)createDemoTextView{
    demoTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 290, 200)];
    
    return demoTextView;
    
}
#pragma mark Prase for Feedback


- (void)savePost:(NSString *)text  Picture:(UIImage *)picture Name:(NSString*)name
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
   
    PFFile *filePicture = nil;
    //---------------------------------------------------------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------------------------------------------------
    if (picture != nil)
    {
       // text = @"[Picture message]";
        filePicture = [PFFile fileWithName:@"picture.jpg" data:UIImageJPEGRepresentation(picture, 0.6)];
        [filePicture saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
         {
             if (error != nil) [ProgressHUD showError:@"Picture save error."];
         }];
    }
    //---------------------------------------------------------------------------------------------------------------------------------------------
    PFObject *object = [PFObject objectWithClassName:PF_POST_CLASS_NAME];
    //object[PF_POST_USER] = [PFUser currentUser];
    object[PF_POST_USER_NAME] = name;
   
    object[PF_POST_CONTENT] = text;
   // object[PF_FEEDBACK_Like_Id] = [NSString stringWithFormat:@"%lu",(unsigned long)no_Oflike];
    if (filePicture != nil)
        object[PF_POST_USER_IMAGE] = filePicture;
    [object pinInBackground];
    
    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
     {
         if (error == nil)
         {
             [self fetchData];
             
         }
         else [ProgressHUD showError:@"Network error."];;
     }];
    //------------------------------------------------------------------------------------------------
}


-(void)fetchData{
    PFQuery *query = [PFQuery queryWithClassName:PF_POST_CLASS_NAME];
    NSLog(@"%@",[PFUser currentUser]);
    [query includeKey:PF_POST_OBJECT_ID];
    
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
     {
         if (error == nil)
         {
             if (objects.count !=0) {
                 [textArray removeAllObjects];
                 [textArray addObjectsFromArray:objects];
                 no_Oflike = textArray.count;
                  [_liveFeedTableView reloadData];
             }
             
         }
         else [ProgressHUD showError:@"Network error."];
         [self.refreshControl endRefreshing];
     }];
}

@end
