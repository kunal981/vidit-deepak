//
//  CommentViewController.m
//  VIDIT
//
//  Created by brst on 9/8/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "CommentViewController.h"
#import "CustomIOSAlertView.h"
#import "AppConstant.h"
#import "ProgressHUD.h"
#include "CommentTableViewCell.h"

@interface CommentViewController ()<CustomIOSAlertViewDelegate>

@end

@implementation CommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
    [_commentTableView registerNib:[UINib nibWithNibName:@"CommentTableViewCell" bundle:nil] forCellReuseIdentifier:@"myCell"];
    _commentTableView.separatorColor = [UIColor clearColor];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Comment" style:UIBarButtonItemStyleBordered target:self action:@selector(commentPress)];
    

    // Do any additional setup after loading the view from its nib.
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    [textView becomeFirstResponder];
}

-(void)commentPress{
    [self customAlert];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self getComment];
    [self customAlert];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return commentArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //[anarray objectAtIndex:(anarray.count - indexPath.row - 1)];
    [cell getData:commentArray[ commentArray.count-indexPath.row-1]];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)customAlert{
    // Here we need to pass a full frame
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    
    // Add some custom content to the alert view
    [alertView setContainerView:[self createDemoTextView]];
    
    // Modify the parameters
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Cancel", @"Comment", nil]];
    [alertView setDelegate:self];
    
    // You may use a Block, rather than a delegate.
    //    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
    //        NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
    //        [alertView close];
    //    }];
    
    [alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [alertView show];
    
}

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", (int)buttonIndex, (int)[alertView tag]);
    if (buttonIndex ==0) {
        [alertView close];
    }
    else if (buttonIndex ==1){
        
        NSString *str  = demoTextView.text;
        PFUser *user = [PFUser currentUser];
        PFFile *file = user[PF_USER_PICTURE];
        if (file==nil) {
            imageView = nil;
            imageView.image =nil;
            // [self actionChat:recent[PF_RECENT_GROUPID]];
        }else {
            
            [file getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                image_user = [UIImage imageWithData:data];
                NSString *name = user[PF_USER_FULLNAME];
                
                [self saveComment:str Picture:image_user Name:name];
                
                
            }];
        }
        //[textArray addObject:str];
        
        [alertView close];
    }
    
}
- (void)saveComment:(NSString *)text  Picture:(UIImage *)picture Name:(NSString*)name
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    
    PFFile *filePicture = nil;
    //---------------------------------------------------------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------------------------------------------------
    if (picture != nil)
    {
        // text = @"[Picture message]";
        filePicture = [PFFile fileWithName:@"picture.jpg" data:UIImageJPEGRepresentation(picture, 0.6)];
        [filePicture saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
         {
             if (error != nil) [ProgressHUD showError:@"Picture save error."];
         }];
    }
    //---------------------------------------------------------------------------------------------------------------------------------------------
    PFObject *object = [PFObject objectWithClassName:@"Comment"];
    object[@"user_object_id"] = [PFUser currentUser];
    object[@"user_name"] = name;
    object [@"post_object_id"] = self.objID;
    
    object[@"content"] = text;
    // object[PF_FEEDBACK_Like_Id] = [NSString stringWithFormat:@"%lu",(unsigned long)no_Oflike];
    if (filePicture != nil)
        object[@"user_image"] = filePicture;
    [object pinInBackground];
    
    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
     {
         if (error == nil)
         {
             //[self fetchData];
             [self getComment];
             
         }
         else [ProgressHUD showError:@"Network error."];;
     }];
    //------------------------------------------------------------------------------------------------
}

-(void)getComment{
    
    PFQuery *query = [PFQuery queryWithClassName:@"Comment"];
    
    [query whereKey:@"post_object_id" equalTo:self.objID];
    
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
     {
         if (error == nil)
         {
             if (objects.count !=0) {
                 
                 NSLog(@"%@",objects);
                 commentArray = objects;
                 [self.commentTableView reloadData];
                 //cell.lblLikes.text = [NSString stringWithFormat:@"%lu Likes",(unsigned long)objects.count];
             }
             
         }
         // [ProgressHUD showError:@"Network error."];
     }];
    

    
}



-(UITextView *)createDemoTextView{
    demoTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 290, 200)];
    demoTextView.delegate  = self;
    return demoTextView;
    
}


- (IBAction)btnCommentPressed:(id)sender {
}
@end
