//
//  CommentViewController.h
//  VIDIT
//
//  Created by brst on 9/8/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface CommentViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>{
   UITextView* demoTextView;
    UIImageView* imageView;
    UIImage *image_user;
    NSArray *commentArray;
}
@property (nonatomic,strong)PFObject *objID;
@property (nonatomic,strong)PFFile *user_image;
@property (nonatomic,strong) NSString *user_name;
- (IBAction)btnCommentPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *commentTableView;
@end
