//
//  ForgotViewController.m
//  VIDIT
//
//  Created by brst on 27/07/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface SettingsView : UITableViewController <UIActionSheetDelegate, UIImagePickerControllerDelegate>{
    BOOL wallpaper;
    UITextView *demoTextView;
    NSMutableArray *textArray;
    NSMutableArray *likeArray;
   // UITextView *txtView;
    UIView *cellView;
   // UIImageView *imageView;
   // UILabel *labelUserName;
    UIImage *image_user;
    NSUInteger no_Oflike;
    
}
//-------------------------------------------------------------------------------------------------------------------------------------------------
@property (strong, nonatomic) IBOutlet UIImageView *imgView_wallpaper;
@property (strong, nonatomic) IBOutlet UITextView *txtView;
- (IBAction)btn_addwallpaperClick:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *labelUserName;

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UITableViewCell *CustomChatCell;
@property (strong, nonatomic) IBOutlet UIButton *btnAdd;
@end
