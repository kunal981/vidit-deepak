//
//  CommentTableViewCell.h
//  VIDIT
//
//  Created by brst on 9/15/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>


@interface CommentTableViewCell : UITableViewCell{
     PFObject *recent;
}
@property (strong, nonatomic) IBOutlet UIImageView *userImageView;
@property (strong, nonatomic) IBOutlet UILabel *lblUserName;
@property (strong, nonatomic) IBOutlet UILabel *lblUserComment;
- (void)getData:(PFObject *)recent_;
@property (strong, nonatomic) IBOutlet PFImageView *imageUser;
@end
