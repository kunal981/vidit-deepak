//
//  ForgotViewController.m
//  VIDIT
//
//  Created by brst on 27/07/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SelectSingleView.h"
#import "SelectMultipleView.h"
#import "AddressBookView.h"
//#import "FacebookFriendsView.h"


//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface RecentView : UITableViewController <UIActionSheetDelegate, SelectSingleDelegate, SelectMultipleDelegate, AddressBookDelegate>
//-------------------------------------------------------------------------------------------------------------------------------------------------
@property (nonatomic, strong) UIImage *imgView;
- (void)loadRecents;

@end
