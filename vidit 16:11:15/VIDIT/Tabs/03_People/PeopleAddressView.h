//
//  PeopleAddressView.h
//  VIDIT
//
//  Created by brst on 20/07/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import <Parse/Parse.h>



//-------------------------------------------------------------------------------------------------------------------------------------------------
@protocol AddressBookDelegate
//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)didSelectAddressBookUser:(PFUser *)user;

@end

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface PeopleAddressView : UITableViewController <UIActionSheetDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate,UISearchBarDelegate,UISearchDisplayDelegate>
//-------------------------------------------------------------------------------------------------------------------------------------------------

@property (nonatomic, assign) IBOutlet id<AddressBookDelegate>delegate;



@end
