//
//  ForgotViewController.m
//  VIDIT
//
//  Created by brst on 27/07/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "JSQMediaItem.h"

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface PhotoMediaItem : JSQMediaItem <JSQMessageMediaData, NSCoding, NSCopying>
//-------------------------------------------------------------------------------------------------------------------------------------------------

@property (copy, nonatomic) UIImage *image;
@property (nonatomic, strong) NSNumber *width;
@property (nonatomic, strong) NSNumber *height;

- (instancetype)initWithImage:(UIImage *)image Width:(NSNumber *)width Height:(NSNumber *)height;

@end
