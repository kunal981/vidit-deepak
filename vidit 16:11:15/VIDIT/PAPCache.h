//
//  PAPCache.h
//  VIDIT
//
//  Created by brst on 9/8/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
@interface PAPCache : NSObject
+ (id)sharedCache;

- (void)clear;

- (void)setAttributesForPost:(PFObject *)Post likers:(NSArray *)likers commenters:(NSArray *)commenters likedByCurrentUser:(BOOL)likedByCurrentUser;

- (NSDictionary *)attributesForPost:(PFObject *)Post;
- (NSNumber *)likeCountForPost:(PFObject *)Post;
- (NSNumber *)commentCountForPost:(PFObject *)Post;
- (NSArray *)likersForPost:(PFObject *)Post;
- (NSArray *)commentersForPost:(PFObject *)Post;
- (void)setPostIsLikedByCurrentUser:(PFObject *)Post liked:(BOOL)liked;
- (BOOL)isPostLikedByCurrentUser:(PFObject *)Post;
- (void)incrementLikerCountForPost:(PFObject *)Post;
- (void)decrementLikerCountForPost:(PFObject *)Post;
- (void)incrementCommentCountForPost:(PFObject *)Post;
- (void)decrementCommentCountForPost:(PFObject *)Post;

- (NSDictionary *)attributesForUser:(PFUser *)user;
- (NSNumber *)PostCountForUser:(PFUser *)user;
- (BOOL)followStatusForUser:(PFUser *)user;
- (void)setPostCount:(NSNumber *)count user:(PFUser *)user;
- (void)setFollowStatus:(BOOL)following user:(PFUser *)user;

- (void)setFacebookFriends:(NSArray *)friends;
- (NSArray *)facebookFriends;

@end
