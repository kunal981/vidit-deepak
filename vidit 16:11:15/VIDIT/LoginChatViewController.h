//
//  LoginChatViewController.h
//  VIDIT
//
//  Created by brst on 08/07/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginChatViewController : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIButton *btnLogin;
- (IBAction)btnLoginPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *fieldEmail;
@property (strong, nonatomic) IBOutlet UITextField *fieldPassword;
@property (strong, nonatomic) IBOutlet UIButton *btnRegister;
- (IBAction)btnRegisterPressed:(id)sender;

- (IBAction)btnForgotPassword:(id)sender;


@end
