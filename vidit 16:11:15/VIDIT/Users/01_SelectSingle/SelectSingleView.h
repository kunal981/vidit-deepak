//
//  ForgotViewController.m
//  VIDIT
//
//  Created by brst on 27/07/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <Parse/Parse.h>

//-------------------------------------------------------------------------------------------------------------------------------------------------
@protocol SelectSingleDelegate
//-------------------------------------------------------------------------------------------------------------------------------------------------

- (void)didSelectSingleUser:(PFUser *)user;

@end

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface SelectSingleView : UITableViewController <UISearchBarDelegate>
//-------------------------------------------------------------------------------------------------------------------------------------------------

@property (nonatomic, assign) IBOutlet id<SelectSingleDelegate>delegate;

@end
