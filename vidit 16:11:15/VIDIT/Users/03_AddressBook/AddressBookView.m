//
//  ForgotViewController.m
//  VIDIT
//
//  Created by brst on 27/07/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <AddressBook/AddressBook.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>

#import <Parse/Parse.h>
#import "ProgressHUD.h"

#import "AppConstant.h"

#import "AddressBookView.h"
#import "constant.h"


//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface AddressBookView()
{
	NSMutableArray *users1;
	NSMutableArray *users2;
    NSMutableArray *filteredArr;
    NSMutableArray *filterContactArray;
	NSIndexPath *indexSelected;
    UISearchBar *searchBar;
    UISearchDisplayController *searchDisplayController;
    NSMutableArray *inviationArray;
    NSDictionary *searchDict;
}
@end
//-------------------------------------------------------------------------------------------------------------------------------------------------

@implementation AddressBookView

@synthesize delegate;

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)viewDidLoad
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
   // vc = [[ForgotViewController alloc]init];
    
	[super viewDidLoad];
	self.title = @"Address Book";
	//---------------------------------------------------------------------------------------------------------------------------------------------
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self
																						  action:@selector(actionCancel)];
	//---------------------------------------------------------------------------------------------------------------------------------------------
    searchBar=[[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    searchBar.placeholder=@"Search";
    //searchBar.userInteractionEnabled=NO;
    searchBar.delegate=self;
    
    searchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:searchBar contentsController:self];
    searchDisplayController.delegate = self;
    searchDisplayController.searchResultsDataSource = self;
    [searchDisplayController setSearchResultsDelegate:self];
    [searchBar setShowsScopeBar:YES];
    
   self.tableView.tableHeaderView = searchBar;
    
	users1 = [[NSMutableArray alloc] init];
	users2 = [[NSMutableArray alloc] init];
    filterContactArray  = [[NSMutableArray alloc]init];
    inviationArray = [[NSMutableArray alloc]init];
    
	//---------------------------------------------------------------------------------------------------------------------------------------------
	ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, nil);
	ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error)
	{
		dispatch_async(dispatch_get_main_queue(), ^{
			if (granted)
                [self loadAddressBook];
            for (NSDictionary *user in users1)
            {
                
              //  NSString *name =  user[@"name"];
                
                [filterContactArray addObject:user];
                    
                
            }
          
           
		});
	});
    
}



#pragma mark - Backend methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)loadAddressBook
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    
	if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized)
	{
		CFErrorRef *error = NULL;
		ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
		ABRecordRef sourceBook = ABAddressBookCopyDefaultSource(addressBook);
		CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeopleInSourceWithSortOrdering(addressBook, sourceBook, kABPersonFirstNameProperty);
		CFIndex personCount = CFArrayGetCount(allPeople);

		[users1 removeAllObjects];
		for (int i=0; i<personCount; i++)
		{
			ABMultiValueRef tmp;
			ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);

			NSString *first = @"";
			tmp = ABRecordCopyValue(person, kABPersonFirstNameProperty);
			if (tmp != nil) first = [NSString stringWithFormat:@"%@", tmp];

			NSString *last = @"";
			tmp = ABRecordCopyValue(person, kABPersonLastNameProperty);
			if (tmp != nil) last = [NSString stringWithFormat:@"%@", tmp];

			NSMutableArray *emails = [[NSMutableArray alloc] init];
			ABMultiValueRef multi1 = ABRecordCopyValue(person, kABPersonEmailProperty);
			for (CFIndex j=0; j<ABMultiValueGetCount(multi1); j++)
			{
				tmp = ABMultiValueCopyValueAtIndex(multi1, j);
				if (tmp != nil) [emails addObject:[NSString stringWithFormat:@"%@", tmp]];
			}

			NSMutableArray *phones = [[NSMutableArray alloc] init];
			ABMultiValueRef multi2 = ABRecordCopyValue(person, kABPersonPhoneProperty);
			for (CFIndex j=0; j<ABMultiValueGetCount(multi2); j++)
			{
				tmp = ABMultiValueCopyValueAtIndex(multi2, j);
				if (tmp != nil) [phones addObject:[NSString stringWithFormat:@"%@", tmp]];
			}

			NSString *name = [NSString stringWithFormat:@"%@ %@", first, last];
			[users1 addObject:@{@"name":name, @"emails":emails, @"phones":phones}];
          
		}
        
        
        filteredArr = [NSMutableArray arrayWithCapacity:[users1 count]];
        NSLog(@"%@",filteredArr);
		CFRelease(allPeople);
		CFRelease(addressBook);
		[self loadUsers];
	}
}


//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)loadUsers
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	NSMutableArray *emails = [[NSMutableArray alloc] init];
	for (NSDictionary *user in users1)
	{
		[emails addObjectsFromArray:user[@"emails"]];
	}
	//---------------------------------------------------------------------------------------------------------------------------------------------
	PFUser *user = [PFUser currentUser];

	PFQuery *query1 = [PFQuery queryWithClassName:PF_BLOCKED_CLASS_NAME];
	[query1 whereKey:PF_BLOCKED_USER1 equalTo:user];

	PFQuery *query2 = [PFQuery queryWithClassName:PF_USER_CLASS_NAME];
	[query2 whereKey:PF_USER_OBJECTID notEqualTo:user.objectId];
	[query2 whereKey:PF_USER_OBJECTID doesNotMatchKey:PF_BLOCKED_USERID2 inQuery:query1];
	[query2 whereKey:PF_USER_EMAILCOPY containedIn:emails];
	[query2 orderByAscending:PF_USER_FULLNAME];
	[query2 setLimit:1000];
	[query2 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
	{
		if (error == nil)
		{
			[users2 removeAllObjects];
			for (PFUser *user in objects)
			{
				[users2 addObject:user];
				[self removeUser:user[PF_USER_EMAILCOPY]];
			}
			[self.tableView reloadData];
		}
		else [ProgressHUD showError:@"Network error."];
	}];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)removeUser:(NSString *)email_
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	NSMutableArray *remove = [[NSMutableArray alloc] init];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	for (NSDictionary *user in users1)
	{
		for (NSString *email in user[@"emails"])
		{
			if ([email isEqualToString:email_])
			{
				[remove addObject:user];
				break;
			}
		}
	}
	//---------------------------------------------------------------------------------------------------------------------------------------------
	for (NSDictionary *user in remove)
	{
		[users1 removeObject:user];
	}
}

#pragma mark - User actions

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionCancel
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	return 2;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	if (section == 0) return [users2 count];
    if (section == 1){
        if (tableView == self.searchDisplayController.searchResultsTableView) {
            return [filteredArr count];
        } else{
            return [users1 count];
        }
    }
	return 0;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	if ((section == 0) && ([users2 count] != 0)) return @"Registered users";
	if ((section == 1) && ([users1 count] != 0)) return @"Non-registered users";
	return nil;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{   
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
	if (cell == nil) cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	if (indexPath.section == 0)
	{
		PFUser *user = users2[indexPath.row];
		cell.textLabel.text = user[PF_USER_FULLNAME];
		cell.detailTextLabel.text = user[PF_USER_EMAILCOPY];
	}
	if (indexPath.section == 1)
	{
        if (tableView == self.searchDisplayController.searchResultsTableView) {
            NSLog(@"%@",filteredArr);
           cell.textLabel.text = [filteredArr objectAtIndex:indexPath.row];
            
        } else {
            
        
		NSDictionary *user = users1[indexPath.row];
		NSString *email = [user[@"emails"] firstObject];
		NSString *phone = [user[@"phones"] firstObject];
		cell.textLabel.text = user[@"name"];
		cell.detailTextLabel.text = (email != nil) ? email : phone;
        }
	}
	//---------------------------------------------------------------------------------------------------------------------------------------------
	cell.detailTextLabel.textColor = [UIColor lightGrayColor];
	return cell;
}

#pragma mark - Table view delegate



//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	if (indexPath.section == 0)
	{
        if (tableView == self.searchDisplayController.searchResultsTableView ) {
            [self dismissViewControllerAnimated:YES completion:^{
                if (delegate != nil)
                    [delegate didSelectAddressBookUser:users2[indexPath.row]];
            }];        }
        else{
		[self dismissViewControllerAnimated:YES completion:^{
			if (delegate != nil)
                [delegate didSelectAddressBookUser:users2[indexPath.row]];
        }];
	 }
    }
	if (indexPath.section == 1)
	{
        if (tableView == self.searchDisplayController.searchResultsTableView ) {
            [self inviteUserFromSearch:filteredArr[indexPath.row]];
        }else{
		indexSelected = indexPath;
		[self inviteUser:users1[indexPath.row]];
    }
	}
}

#pragma mark Content Filtering
-(void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
    // Update the filtered array based on the search text and scope.
    // Remove all objects from the filtered search array
    [filteredArr removeAllObjects];
    // Filter the array using NSPredicate
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",searchText];
    filteredArr = [NSMutableArray arrayWithArray:[[filterContactArray valueForKey:@"name"] filteredArrayUsingPredicate:predicate]];
    [searchDisplayController.searchResultsTableView reloadData];
}

#pragma mark - UISearchDisplayController Delegate Methods
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    // Tells the table data source to reload when text changes
    
    [self filterContentForSearchText:searchString scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    // Tells the table data source to reload when scope bar selection changes
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.tableView reloadData];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    [self.tableView reloadData];
}

#pragma mark - Invite helper method

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)inviteUser:(NSDictionary *)user
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSLog(@"%@",user);
    
	if (([user[@"emails"] count] != 0) && ([user[@"phones"] count] != 0))
	{
		UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel"
											  destructiveButtonTitle:nil otherButtonTitles:@"Email invitation", @"SMS invitation", nil];
        action.tag = 2;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            // In this case the device is an iPad.
            [action showFromRect: CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 200, 200)  inView:self.view animated:YES];
        }
        else{
            // In this case the device is an iPhone/iPod Touch.
            [action showFromTabBar:[[self tabBarController] tabBar]];
            
        }
	}
	else if (([user[@"emails"] count] != 0) && ([user[@"phones"] count] == 0))
	{
        NSLog(@"%@",user);
		[self sendMail:user];
	}
	else if (([user[@"emails"] count] == 0) && ([user[@"phones"] count] != 0))
	{
		[self sendSMS:user];
	}
	else [ProgressHUD showError:@"This contact does not have enough information to be invited."];
}

-(void)inviteUserFromSearch:(NSString*)name{
    NSLog(@"%@",users1);
    NSLog(@"%@",name);
    
    for (int i =0; i<users1.count; i++) {
        NSString *uName = [[users1 objectAtIndex:i]valueForKey:@"name"];
        NSLog(@"%@",uName);
        if ([uName isEqualToString:name]) {
            [self inviteUserSearch:users1[i]];
            

            break;
        }
    }
}

- (void)inviteUserSearch:(NSDictionary *)user
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
   
    searchDict  = [[NSDictionary alloc]initWithDictionary:user];
     NSLog(@"%@",searchDict);
    
    if (([user[@"emails"] count] != 0) && ([user[@"phones"] count] != 0))
    {
        UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel"
                                              destructiveButtonTitle:nil otherButtonTitles:@"Email invitation", @"SMS invitation", nil];
        action.tag= 1;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            // In this case the device is an iPad.
            [action showFromRect: CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 200, 200)  inView:self.view animated:YES];
        }
        else{
            // In this case the device is an iPhone/iPod Touch.
            [action showFromTabBar:[[self tabBarController] tabBar]];
            
        }
    }
    else if (([user[@"emails"] count] != 0) && ([user[@"phones"] count] == 0))
    {
        NSLog(@"%@",user);
        [self sendMail:user];
    }
    else if (([user[@"emails"] count] == 0) && ([user[@"phones"] count] != 0))
    {
        [self sendSMS:user];
    }
    else [ProgressHUD showError:@"This contact does not have enough information to be invited."];
}




#pragma mark - UIActionSheetDelegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	if (buttonIndex == actionSheet.cancelButtonIndex) return;
	//---------------------------------------------------------------------------------------------------------------------------------------------
	NSDictionary *user = users1[indexSelected.row];
    if (buttonIndex == 0 && actionSheet.tag==1){
       
        [self sendMail:searchDict];
    }else if(buttonIndex == 0 && actionSheet.tag==2){
        [self sendMail:user];
 
    }
   else if (buttonIndex == 1 && actionSheet.tag == 1){
        [self sendSMS:searchDict];
        
    }
    else if (buttonIndex == 1 && actionSheet.tag == 2){
        [self sendSMS:user];
    }
}

#pragma mark - Mail sending method

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)sendMail:(NSDictionary *)user
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSLog(@"%@",user);
	if ([MFMailComposeViewController canSendMail])
	{
		MFMailComposeViewController *mailCompose = [[MFMailComposeViewController alloc] init];
		[mailCompose setToRecipients:user[@"emails"]];
		[mailCompose setSubject:@""];
		[mailCompose setMessageBody:MESSAGE_INVITE isHTML:YES];
		mailCompose.mailComposeDelegate = self;
		[self presentViewController:mailCompose animated:YES completion:nil];
	}
	else [ProgressHUD showError:@"Please configure your mail first."];
}

#pragma mark - MFMailComposeViewControllerDelegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	if (result == MFMailComposeResultSent)
	{
		[ProgressHUD showSuccess:@"Mail sent successfully."];
	}
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - SMS sending method

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)sendSMS:(NSDictionary *)user
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	if ([MFMessageComposeViewController canSendText])
	{
		MFMessageComposeViewController *messageCompose = [[MFMessageComposeViewController alloc] init];
		messageCompose.recipients = user[@"phones"];
		messageCompose.body = MESSAGE_INVITE;
		messageCompose.messageComposeDelegate = self;
		[self presentViewController:messageCompose animated:YES completion:nil];
	}
	else [ProgressHUD showError:@"SMS cannot be sent from this device."];
}

#pragma mark - MFMessageComposeViewControllerDelegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	if (result == MessageComposeResultSent)
	{
		[ProgressHUD showSuccess:@"SMS sent successfully."];
	}
	[self dismissViewControllerAnimated:YES completion:nil];
}

@end
