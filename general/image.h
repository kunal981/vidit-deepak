//
//  ForgotViewController.m
//  VIDIT
//
//  Created by brst on 27/07/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

//-------------------------------------------------------------------------------------------------------------------------------------------------
UIImage*		SquareImage				(UIImage *image, CGFloat size);
UIImage*		ResizeImage				(UIImage *image, CGFloat width, CGFloat height);
UIImage*		CropImage				(UIImage *image, CGFloat x, CGFloat y, CGFloat width, CGFloat height);
