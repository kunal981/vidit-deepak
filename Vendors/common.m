

#import "common.h"
#import "LoginChatViewController.h"

#import "NavigationController.h"

//-------------------------------------------------------------------------------------------------------------------------------------------------
void LoginUser(id target)
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	NavigationController *navigationController = [[NavigationController alloc] initWithRootViewController:[[LoginChatViewController alloc] init]];
	[target presentViewController:navigationController animated:YES completion:nil];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
void ActionPremium(id target)
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
//	PremiumView *premiumView = [[PremiumView alloc] init];
//	premiumView.modalPresentationStyle = UIModalPresentationOverFullScreen;
//	[target presentViewController:premiumView animated:YES completion:nil];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
void PostNotification(NSString *notification)
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[[NSNotificationCenter defaultCenter] postNotificationName:notification object:nil];
}
